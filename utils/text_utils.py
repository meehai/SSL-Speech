# Module used for alphabet definition and general text manipulation functions.
import numpy as np

# General alphabet class (can be letters, phonemes or anything else). It must update the index for the blank char
#  accordingly with the algorithm used later, as well as be careful with indexing
class Alphabet:
	def __init__(self, alphabetFile):
		f = open(alphabetFile, "r")
		self.charMap, self.indexMap = {}, {}
		# 0 for Keras ctc_loss first char (N for blank), 1 for warp ctc (0 is blank)
		i = 0
		for line in f.readlines():
			line = line.strip()
			if line == "<SPACE>":
				line = " "

			# charMap :: char -> uint ("a" => 1)
			self.charMap[line] = i

			# indexMap :: uint -> char (2 => "<SPACE>")
			self.indexMap[i] = line
			i += 1
		# Notation to show blanks when printing results
		self.indexMap[self.getBlankIndex()] = ""
		f.close()

	def getAlphabetSize(self):
		return len(self.charMap.keys()) + 1

	def getBlankIndex(self):
		# 0 for warp_ctc, alphabetSize - 1 for Keras/TF
		return self.getAlphabetSize() - 1

	def sequenceTextToInt(self, text):
		""" Use a character map and convert text to an integer sequence """
		return [self.charMap[c] for c in text]

	def sequenceIntToText(self, tokens):
		return ''.join([self.indexMap[i] for i in tokens])

	def replaceCharWithBlank(self, prediction, value):
		for i in range(len(prediction)):
			if prediction[i] == value:
				prediction[i] = self.getBlankIndex()
		return prediction

def character_error_rate(ref, hyp):
	import editdistance
	return editdistance.eval(ref, hyp) / len(ref)

# https://martin-thoma.com/word-error-rate-calculation/
def word_error_rate(ref, hyp):
	"""
	Calculation of WER with Levenshtein distance.

	Works only for iterables up to 254 elements (uint8).
	O(nm) time ans space complexity.

	Parameters
	----------
	ref : sentence as string
	hyp : sentence as string

	Returns
	-------
	int

	Examples
	--------
	>>> wer("who is there", "is there")
	1
	>>> wer("who is there", "")
	3
	>>> wer("", "who is there")
	3
	"""

	# split in words
	r = ref.split()
	h = hyp.split()

	# initialisation

	d = np.zeros((len(r)+1)*(len(h)+1), dtype='uint8')
	d = d.reshape((len(r)+1, len(h)+1))
	for i in range(len(r)+1):
		for j in range(len(h)+1):
			if i == 0:
				d[0][j] = j
			elif j == 0:
				d[i][0] = i

	# computation
	for i in range(1, len(r)+1):
		for j in range(1, len(h)+1):
			if r[i-1] == h[j-1]:
				d[i][j] = d[i-1][j-1]
			else:
				substitution = d[i-1][j-1] + 1
				insertion    = d[i][j-1] + 1
				deletion     = d[i-1][j] + 1
				d[i][j] = min(substitution, insertion, deletion)

	return np.clip(d[len(r)][len(h)] / len(r), 0, 1)

# Some sort of singleton for the alphabet
alphabet = Alphabet("alphabet.txt")
