"""
Defines a class that is used to featurize audio clips, and provide
them to the network for training or testing.
"""

import json
import numpy as np
import random
import logging as log

from concurrent.futures import ThreadPoolExecutor, wait
from utils.sound_utils import calc_feat_dim, spectrogram_from_file, sort_by_duration
from utils.text_utils import alphabet
from utils.utils import conv_output_length

RNG_SEED = 123

class DataGenerator(object):
    def __init__(self, step=10, window=20, max_freq=8000, max_duration=10):
        """
        Params:
            step (int): Step size in milliseconds between windows
            window (int): FFT window size in milliseconds
            max_freq (int): Only FFT bins corresponding to frequencies between
                [0, max_freq] are returned
            max_duration (float): In seconds, the maximum duration of
                utterances to train or test on
        """
        self.feat_dim = calc_feat_dim(window, max_freq)
        self.feats_mean = np.zeros((self.feat_dim,))
        self.feats_std = np.ones((self.feat_dim,))
        self.rng = random.Random(RNG_SEED)
        self.step = step
        self.window = window
        self.max_freq = max_freq
        self.max_duration = max_duration
        self.sortagradTrain = False
        self.conv_params = None

    def featurize(self, audio_clip):
        """ For a given audio clip, calculate the log of its Fourier Transform
        Params:
            audio_clip(str): Path to the audio clip
        """
        return spectrogram_from_file(
            audio_clip, step=self.step, window=self.window,
            max_freq=self.max_freq)

    def load_metadata_from_desc_file(self, desc_file, type="train"):
        """ Read metadata from the description file
            (possibly takes long, depending on the filesize)
        Params:
            desc_file (str):  Path to a JSON-line file that contains labels and
                paths to the audio files
            type (str): One of 'train', 'validation' or 'test'
        """
        log.info("Reading description file {} for: {}".format(desc_file, type))
        assert type in ("train", "validation", "test")
        audio_paths, durations, texts = [], [], []
        with open(desc_file) as json_line_file:
            for line_num, json_line in enumerate(json_line_file):
                try:
                    spec = json.loads(json_line)
                    if float(spec['duration']) > self.max_duration:
                        continue
                    audio_paths.append(spec['key'])
                    durations.append(float(spec['duration']))
                    texts.append(spec['text'])
                except Exception as e:
                    # Change to (KeyError, ValueError) or
                    # (KeyError,json.decoder.JSONDecodeError), depending on
                    # json module version
                    log.info("Error reading line #{}: {}\n{}".format(line_num, json_line, str(e)))
        self.setData(type, durations, audio_paths, texts)

    def setData(self, type, durations, audio_paths, texts):
        assert type in ("train", "validation", "test")
        if type == "train":
            self.train_audio_paths, self.train_durations, self.train_texts = audio_paths, durations, texts
        elif type == "validation":
            self.val_audio_paths, self.val_durations, self.val_texts = audio_paths, durations, texts
        else:
            self.test_audio_paths, self.test_durations, self.test_texts = audio_paths, durations, texts        

    def getData(self, type):
        assert type in ("train", "validation", "test")
        if type == "train":
            if self.sortagradTrain:
                return sort_by_duration(self.train_durations, self.train_audio_paths, self.train_texts)
            else:
                return self.train_durations, self.train_audio_paths, self.train_texts
        elif type == "validation":
            return self.val_durations, self.val_audio_paths, self.val_texts
        else:
            return self.test_durations, self. test_audio_paths, self.test_texts

    def load_train_data(self, desc_file):
        self.load_metadata_from_desc_file(desc_file, 'train')

    def load_test_data(self, desc_file):
        self.load_metadata_from_desc_file(desc_file, 'test')

    def load_validation_data(self, desc_file):
        self.load_metadata_from_desc_file(desc_file, 'validation')

    def normalize(self, feature, eps=1e-14):
        return (feature - self.feats_mean) / (self.feats_std + eps)

    def prepare_minibatch(self, audio_paths, texts):
        """ Featurize a minibatch of audio, zero pad them and return a dictionary
        Params:
            audio_paths (list(str)): List of paths to audio files
            texts (list(str)): List of texts corresponding to the audio files
        Returns:
            dict: See below for contents
        """
        assert len(audio_paths) == len(texts),\
            "Inputs and outputs to the network must be of the same number"
        # Features is a list of (timesteps, feature_dim) arrays
        # Calculate the features for each audio clip, as the log of the
        # Fourier Transform of the audio
        features = [self.featurize(a) for a in audio_paths]

        input_lengths = np.array([f.shape[0] for f in features])
        max_length = max(input_lengths)
        # max_length = self.max_time_samples
        feature_dim = features[0].shape[1]
        mb_size = len(features)

        # Pad all the inputs so that they are all the same length
        x = np.zeros((mb_size, max_length, feature_dim), dtype="float32")

        label_lengths = np.zeros((mb_size, ), dtype="int32")

        y = []
        for i in range(mb_size):
            feat = features[i]
            feat = self.normalize(feat)  # Center using means and std
            x[i, :feat.shape[0], :] = feat
            label = alphabet.sequenceTextToInt(texts[i])
            y.append(label)
            label_lengths[i] = len(label)
            for conv_param in self.conv_params:
                input_lengths[i] = conv_output_length(input_lengths[i], *conv_param)

        label_max_len = np.max(label_lengths)
        # Pad the labels too and expect that 0 is the null character in the text.
        labels = alphabet.getBlankIndex() * np.ones((mb_size, label_max_len, ), dtype="int32")

        for i in range(mb_size):
            labels[i, 0 : label_lengths[i]] = np.array(y[i], dtype="int32")

        inputs = [x, labels, input_lengths, label_lengths]
        outputs = np.zeros(mb_size) # dummy data for dummy loss function

        return (inputs, outputs)

    def getIterationsCount(self, type, miniBatchSize):
        assert type in ("test", "train", "validation")
        _, audio_paths, _ = self.getData(type)
        return int(np.ceil(len(audio_paths) / miniBatchSize)) - 1

    '''
    def iterate(self, audio_paths, texts, miniBatchSize, type, k_iters=None):
        assert type in ("train", "test", "validation")
        if k_iters is None:
            k_iters = self.getIterationsCount(type, miniBatchSize)
        pool = ThreadPoolExecutor(1)  # Run a single I/O thread in parallel
        future = pool.submit(self.prepare_minibatch,
                             audio_paths[:miniBatchSize],
                             texts[:miniBatchSize])
        start = miniBatchSize
        for i in range(k_iters - 1):
            wait([future])
            minibatch = future.result()
            # While the current minibatch is being consumed, prepare the next
            future = pool.submit(self.prepare_minibatch,
                                 audio_paths[start: start + miniBatchSize],
                                 texts[start: start + miniBatchSize])
            yield minibatch
            start += miniBatchSize
        # Wait on the last minibatch
        wait([future])
        minibatch = future.result()
        yield minibatch
    '''

    # Same as iterate but without aditional thread.
    # iterationsCount. The number of iterations in an epoch. Every epoch uses the same data. For correct usage, this
    #  should be the same value as steps_per_epoch parameter (this holds for None)
    def iterate_single(self, type, miniBatchSize, iterationsCount=None):
        assert type in ("train", "test", "validation")
        if iterationsCount is None:
            iterationsCount = self.getIterationsCount(type, miniBatchSize)

        while True:
            start = 0
            durations, audio_paths, texts = self.getData(type)
            prev_result = []

            for i in range(iterationsCount):
                result = self.prepare_minibatch(audio_paths[start : start + miniBatchSize], \
                    texts[start : start + miniBatchSize])
                start += miniBatchSize

                (x, numeric_labels, input_length, label_length), _ = result
                # Test only needs these 3 items where labels are the "true values" used for other processing
                if type == "test":
                    result = (x, input_length), numeric_labels

                del prev_result
                yield result
                prev_result = result

            # Only train/validate can go on for multiple epochs.
            if type == "test":
                log.info("Testing complete.")
                break

    def fit_train(self, k_samples=100):
        """ Estimate the mean and std of the features from the training set
        Params:
            k_samples (int): Use this number of samples for estimation
        """
        k_samples = min(k_samples, len(self.train_audio_paths))
        samples = self.rng.sample(self.train_audio_paths, k_samples)
        feats = [self.featurize(s) for s in samples]
        feats = np.vstack(feats)
        self.feats_mean = np.mean(feats, axis=0)
        self.feats_std = np.std(feats, axis=0)
