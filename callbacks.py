import sys
import os
import numpy as np
import logging as log
from keras.callbacks import Callback
from keras import backend as K
from utils.text_utils import alphabet, character_error_rate, word_error_rate

# Records the loss progress and writes in file every count batches
class LossHistory(Callback):
	def __init__(self, file, logEveryBatchCount=1):
		self.file = open(file, "w")
		self.logEveryBatchCount = logEveryBatchCount
		self.sumLoss = 0
		self.countLoss = 0

	def on_batch_end(self, batch, logs={}):
		if batch % self.logEveryBatchCount == 0:
			self.file.write("Batch: {}, Loss: {}\n".format(batch, logs["loss"]))
			self.file.flush()
			os.fsync(self.file.fileno())

		self.sumLoss += logs["loss"]
		self.countLoss += 1

	def on_epoch_begin(self, epoch, logs={}):
		self.file.write("Epoch: {}\n".format(epoch))

	def on_epoch_end(self, epoch, logs={}):
		self.file.write("Average loss on epoch {}: {}\n".format(epoch, self.sumLoss / self.countLoss))
		self.sumLoss = 0
		self.countLoss = 0


class SaveModelCallback(Callback):
	def __init__(self, model):
		self.model = model

	def on_epoch_end(self, epoch, logs={}):
		self.model.save("model_epoch{}.h5".format(epoch))

		if "val_loss" in logs:
			log.info("Epoch: {}. Validation Loss: {}. Model saved.".format(epoch, logs["val_loss"]))
		else:
			log.info("Epoch: {}. No validation data for loss. Model saved.".format(epoch))

# Sets the sortagrad to False after the first epoch.
class SortaGradCallback(Callback):
	def __init__(self, datagen):
		self.datagen = datagen

	def on_epoch_begin(self, epoch, logs={}):
		if epoch > 0:
			self.datagen.sortagradTrain = False

# Custom evaluation callback to write results to a file
class EvaluateCallback():
	def __init__(self, decode_types, evaluation_types, file):
		for type in decode_types:
			assert type in ("ctc_decode_greedy", "ctc_decode_beam_search", "lm_decoder")
		for type in evaluation_types:
			assert type in ("character_error_rate", "word_error_rate")

		# Map that will store different results throughout the evaluation.
		# results["character_error_rate"] = {"ctc_decode_greedy" : <Value>, "ctc_decode_beam_search" : <Value> }
		# results["word_error_rate"] = { ... }
		self.results = {}

		# Initialize the values of CER
		if "character_error_rate" in evaluation_types:
			self.results["CER"] = {}
		if "word_error_rate" in evaluation_types:
			self.results["WER"] = {}

		self.decode_types = decode_types
		self.evaluation_types = evaluation_types
		self.file = open(file, "w")
		self.evaluationCount = 0 # Number of utterances that were analyzed

	'''
	def argmax_decode(self, predictions, input_length):
		numPredictions = predictions.shape[0]
		decoded_predictions = list(map(lambda x : argmax_decode(x, alphabet.getBlankIndex()), predictions))
		return decoded_predictions
	'''

	def ctc_decode_greedy(self, predictions, input_length):
		numPredictions = predictions.shape[0]
		if len(input_length.shape) == 2 and input_length.shape[1] == 1:
			input_length = input_length.reshape(input_length.shape[0], )

		predictions, _ = K.ctc_decode(predictions, input_length, greedy=True)
		new_predictions = predictions[0].eval(session = K.get_session())

		# Remove the -1 with the blank character that CTC places.
		decoded_predictions = list(map(lambda inner_list : alphabet.replaceCharWithBlank(inner_list, -1), \
			new_predictions))
		return decoded_predictions

	def ctc_decode_beam_search(self, predictions, input_length):
		numPredictions = predictions.shape[0]
		top_paths = 2 # Change this if we want multiple paths to be saved (not just best one)
		if len(input_length.shape) == 2 and input_length.shape[1] == 1:
			input_length = input_length.reshape(input_length.shape[0], )

		predictions, _ = K.ctc_decode(predictions, input_length, greedy=False, top_paths=top_paths)
		top_predictions = []

		for i in range(len(predictions)):
			new_predictions = predictions[i].eval(session = K.get_session())
			# Remove the -1 with the blank character that CTC places.
			decoded_predictions = list(map(lambda inner_list : alphabet.replaceCharWithBlank(inner_list, -1), \
				new_predictions))
			top_predictions.append(decoded_predictions)
		#top_predictions = np.array(top_predictions)
		#top_predictions = np.swapaxes(top_predictions, 0, 1)
		return top_predictions

	# predictions, a matrix of minibatch_size x time_steps x softmax_results
	# input_lengths, an array for the size of each utterance, shape minibatch_size x 1
	# labels, an array containing the true labels, shape minibatch_size x time_steps or None if not available
	# if labels is None, we don't know the true value, just print the predicted value, otherwise, print both.
	def __call__(self, predictions, input_length, labels=None):
		# Assert the same amount of predictions, lengths and labels are given
		assert predictions.shape[0] == input_length.shape[0] and \
			(labels is None or predictions.shape[0] == labels.shape[0])
		# Assert that if CER or WER (or others) are computed, there exists some labels.
		if self.evaluation_types != []:
			assert not labels is None

		# Add these utterances to the count for WER, CER etc.
		self.evaluationCount += len(predictions)
		# Save the results for all desired types as a temporary map
		new_predictions = {}
		#if "argmax_decode" in self.decode_types:
		#	new_predictions["Argmax decode"] = self.argmax_decode(predictions, input_length)

		if "ctc_decode_greedy" in self.decode_types:
			new_predictions["CTC greedy"] = self.ctc_decode_greedy(predictions, input_length)
		if "ctc_decode_beam_search" in self.decode_types:
			ctc_beam_search_predictions = self.ctc_decode_beam_search(predictions, input_length)
			numPredictions = len(ctc_beam_search_predictions)
			for i in range(numPredictions):
				new_predictions["CTC beam search " + str(i)] = ctc_beam_search_predictions[i]

		# Write the results to the file and call the other evaluation metrics if any (CER).
		for i in range(len(predictions)):
			decoded_label = None
			# Print the labels, if any.
			if labels is not None:
				decoded_label = alphabet.sequenceIntToText(labels[i])
				self.file.write("True value: {}\n".format(decoded_label))

			# Print the decodings, if any.
			for key in new_predictions:
				decoded = alphabet.sequenceIntToText(new_predictions[key][i])
				self.file.write("{}: {}".format(key, decoded))

				# Store the aditional processing (CER, WER) for this particular sentence, if any.
				# CER
				if "character_error_rate" in self.evaluation_types:
					if not key in self.results["CER"]:
						self.results["CER"][key] = 0
					cer = character_error_rate(decoded_label, decoded)
					self.file.write(" (CER: {0:.2f})".format(cer))
					self.results["CER"][key] += cer

				# WER
				if "word_error_rate" in self.evaluation_types:
					if not key in self.results["WER"]:
						self.results["WER"][key] = 0
					wer = word_error_rate(decoded_label, decoded)
					self.file.write(" (WER: {0:.2f})".format(wer))
					self.results["WER"][key] += wer

				self.file.write("\n")

			self.file.write("\n")
			self.file.flush()
			os.fsync(self.file.fileno())

	# Print the aditional processing and close the resources.
	def on_end(self):
		if "character_error_rate" in self.evaluation_types:
			log.info("Character error rate:")
			resultsMap = self.results["CER"]
			for key in resultsMap:
				log.info("{}: {}".format(key, resultsMap[key] / self.evaluationCount))

		if "word_error_rate" in self.evaluation_types:
			log.info("Word error rate:")
			resultsMap = self.results["WER"]
			for key in resultsMap:
				log.info("{}: {}".format(key, resultsMap[key] / self.evaluationCount))

		self.file.close()
